/* 
Abdul Rafay Javed
BESE-4B
07504
*/

#include <stdio.h>


typedef void(*newMatrix)(struct Matrix *mat, int rows, int columns);

typedef float(*sum)(struct Matrix *mat1, struct Matrix *mat2);

typedef float(*product)(struct Matrix *mat1, struct Matrix *mat2);

typedef void(*newVector)(int rows, struct Vector *vec);

typedef void(*L1Norm)(struct Matrix *mat);


typedef struct Matrix {

	int rows;
	int columns;
	int **matrix;
	newMatrix newMat;
	product prod;
	sum sum_func;
	L1Norm *ptr_vtable;

	
  
} Mat;

float product_matrix(struct Matrix *mat1, struct Matrix *mat2)
{
	
	int result[3][3];

	int a;
	int b;
	int c;
	int sum;
	for (a = 0; a < mat1 -> rows; a++) {
		for (b = 0; b < mat2 -> columns; b++) {
			for (c = 0; c < mat2 -> rows; c++) {
				sum = sum + ((mat1->matrix[a][c]) * (mat2->matrix[c][b]));
			}

			result[a][b] = sum;
			sum = 0;
		}
	}
	

	for (a = 0; a < mat1->rows ; a++) {
		for (b = 0; b < mat2 -> columns; b++){
			printf("%d\t", result[a][b]);
		}

		printf("\n");
	}
}


float addition_matrix(struct Matrix *mat1, struct Matrix *mat2){

	
	int a;
	int b;
	int result[3][3];
	for (a = 0; a < mat1->rows; a++)
	{
		for (b = 0; b < mat1->columns; b++){
			result[a][b] = mat1->matrix[a][b] + mat2->matrix[a][b];
			printf("%d\t",result[a][b]);
		}
		printf("\n");
	}
}


void cal_L1Norm_matrix (struct Matrix *mat){
    
	int var = 0;
    
	int L1[20];
	int a;
	int b;
	
	
	for (a = 0; a < mat->columns; a++) {
		L1[a] = 0;
		for (b = 0; b < mat->rows; b++) {
			L1[a] += mat->matrix[b][a];
		}
	}


	int c;

	for (c = 0; c < mat -> columns; c++){
		if (L1[c] > var)
			var = L1[c];
	}

	printf("Matrix L1Norm  %d\n", var);
    return;
}

void newMatrix_init(struct Matrix *matrix, int rows, int columns){

	Mat mat;
	mat.rows = rows;
	mat.columns = columns;
	mat.matrix = malloc(rows * sizeof(int *));
	mat.prod = product_matrix;
	mat.sum_func = addition_matrix;

	int i = 0;
	for (i = 0; i < rows; i++)
	{
		mat.matrix[i] = malloc(columns * sizeof(int));
	}
	for (i = 0; i < 3; i++)
	{
		int k;
		for (k = 0; k < 3; k++)
			mat.matrix[i][k] = 5;
	}

	*matrix = mat;
}

typedef struct Vector {
	Mat vector;
	newVector vect_init;
} Vec;

void cal_L1Norm_vector (struct Matrix *mat){
    int var=0;
    int a;	
    for(a = 0 ; a< mat->rows; a++){
        var+=	mat->matrix[a][0];
    }
	printf("Vector L1Norm  %d\n", var);
    return;
}


void newVector_init(int rows, struct Vector *vec){
	Vec vector;
	vector.vector.newMat = newMatrix_init;
	vector.vector.newMat(&vector.vector, rows, 1);
	*vec = vector;
}


int main() {

	printf("\n            **************** Task 1 ****************\n");	
	Vec vector;
	vector.vect_init = newVector_init;
	vector.vect_init(3, &vector);
	printf("Vector:\n");

	int a;
	int b;
	for (a = 0; a < vector.vector.rows ; a++)
	{
		for (b = 0; b < vector.vector.columns ; b++)
			printf("%d\t", vector.vector.matrix[a][b]);
		printf("\n");
	}

	Mat matrix1;
	matrix1.newMat = newMatrix_init;
	matrix1.newMat(&matrix1, 3, 3);

	Mat matrix2;
	matrix2.newMat = newMatrix_init;
	matrix2.newMat(&matrix2, 3, 3);

	
	printf("Sum:\n");
	matrix1.sum_func(&matrix1, &matrix2);
	printf("Product:\n");
	matrix1.prod(&matrix1, &matrix2);

	printf("\n            **************** Task 2 ****************\n");

	L1Norm *vtable_class = malloc(sizeof(L1Norm));
    	vtable_class[0] = &cal_L1Norm_matrix;
	L1Norm *vtable_class1 = malloc(sizeof(L1Norm));
	vtable_class1[0] = &cal_L1Norm_vector;

	Mat *matrix = (Mat*)malloc(sizeof(Vec));
	matrix->newMat = newMatrix_init;
	matrix->newMat (matrix, 3, 3);
	
	printf("\nAddition\n");
	matrix->sum_func(matrix, matrix);
	printf("\nProduct:\n");
	matrix->prod(matrix, matrix);


	matrix->ptr_vtable = vtable_class;
	printf("\nL1Norm:\n");
	matrix->ptr_vtable[0](matrix);


	 

	return 0;
}


